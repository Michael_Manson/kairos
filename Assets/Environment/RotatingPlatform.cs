﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatingPlatform : MonoBehaviour
{
    public Transform customPivot;
    [SerializeField] private float RotationSpeed = 1.0f;

    // Update is called once per frame
    void Update()
    {
        Quaternion q = Quaternion.FromToRotation(transform.up, Vector3.up) * transform.rotation;
        transform.rotation = Quaternion.Slerp(transform.rotation, q, Time.deltaTime * RotationSpeed);
        transform.RotateAround(customPivot.position, Vector3.forward, RotationSpeed * Time.deltaTime);
    }

}
