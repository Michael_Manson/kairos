﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    public void Start()
    { //when the menu is started cursor is unlocked and visible
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }
    //the following load absolute reference scenes 
    public void PlayGame()
    {
        SceneManager.LoadScene("Main");
    }

    public void QuitGame()
    {
        //ends the game
        Application.Quit();

        //While in editor // TO BE REMOVED // 
        //UnityEditor.EditorApplication.isPlaying = false;
    }
}
