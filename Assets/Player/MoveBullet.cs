﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveBullet : MonoBehaviour
{
    Vector3 moveDirection;
    private Vector3 spawnPosition;
    public float bulletRange;

    public float bulletSpeed = 20f;         //the speed of the bullet

    Vector3 increaseScale = new Vector3( 0.01f, 0.01f, 0.01f );

    private GameObject owner;

    [SerializeField] private int damage = 10;

    // Start is called before the first frame update
    void Start()
    {
        spawnPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        //Vector3 moveDirection = new Vector3(transform.position.x, transform.position.y, 0.0f).normalized;
        //transform.position += moveDirection * bulletSpeed * Time.deltaTime;

        //just for fun
        //
        //transform.localScale *= 1.025f;
        //bulletSpeed -= 1f;
        //


        if (moveDirection != Vector3.zero)
        {
            transform.position += moveDirection * bulletSpeed * Time.deltaTime;
        }

        //the below catches the case where the joystick hasn't been moved yet, which was nulling out the whole movement equation
        else 
        {
            transform.position += new Vector3(1f, 0f, 0f) * bulletSpeed * Time.deltaTime;
        }
        

        //if the bullet's position exceeds its range, destroy self
        float distancesincespawn = (transform.position - spawnPosition).magnitude;
        if (distancesincespawn >= bulletRange)
        {
            Object.DestroyImmediate(gameObject);
        }
    }

    public void SetParameters(float bulletSpeed, float bulletRange)
    {
        this.bulletSpeed = bulletSpeed;
        this.bulletRange = bulletRange;
    }

    public void SetDirection(Vector3 direction)
    {
        moveDirection = direction.normalized;
    }

    public void SetOwner(GameObject owner)
    {
        this.owner = owner;
    }

    private void OnTriggerEnter(Collider other)
    {
        
        if (other.gameObject.tag == "Shield" && other.GetComponent<DeployShield>().owner != this.owner)
        {
            other.GetComponent<DeployShield>().DeductHealth(damage);
            Destroy(gameObject);
        }
    }
}
