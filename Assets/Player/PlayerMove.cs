using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.InputSystem;
using UnityEngine.Scripting.APIUpdating;

public class PlayerMove : MonoBehaviour
{
    // reference to the controls class
    public Controls controls;
    private CharacterController controller;

    // jump variables
    [SerializeField] private float jumpHeight = 7.0f;
    [SerializeField] private float gravityMultiplier = 4.0f;
    private Vector2 jumpDirection;
    Collider[] colliders;
    private bool grounded = false;


    //movement variables
    [SerializeField] private float moveSpeed = 0f;       //player movement speed
    [SerializeField] private float turnSmoothing = 0.1f;     //how quickly the transform transitions between facing directions (lower = faster)
    Vector2 moveDirection = Vector2.zero;
    private float turnSmoothVelocity;                       //holds current smooth velocity

    bool moving = false;
    [SerializeField] private Vector2 maxMoveSpeed = new Vector2(20.0f, 0);
    [SerializeField] private float acceleration;

    private Vector3 gunDirection;                           //mouse and analogue stick direction
    private Vector2 savedGunDirection;
    [SerializeField] private Transform gun;
    public float gunRecoil = 1.25f;                           //will be set by player, determines force pushing character back when shooting ---> Need to figure out what each level amounts to
    public float airRecoilModifier = 1.5f;                    //scale recoil by this when in the air
    public float fireRate = 0.5f;
    
    bool shooting = false;                                    //set to true while trigger down
    bool allowFire = true;

    public Camera cam;

    //Bullets
    [SerializeField] private GameObject bullet;
    [SerializeField] private GameObject bulletInstantiatePoint;

    //Shield
    [SerializeField] private GameObject shield;
    [SerializeField] private GameObject shieldInstantiatePoint;

    //Melee
    [SerializeField] private GameObject meleeWeapon;
    private bool hasMeleeWeapon = false;
    [SerializeField] private float meleeRate = 0.5f;
    private bool allowMelee = true;

    // Start is called before the first frame update
    void Start()
    {
        controller = GetComponent<CharacterController>();
        controls = new Controls();
        controls.Player.Jump.performed += Jump;
        controls.Player.DeployShield.performed += DeployShield;

        controls.Player.FireWeapon.performed += _ => shooting = true;

        controls.Player.GunDirection.performed += GunDirection;

        controls.Player.Quit.performed += Quit;

        controls.Player.Move.performed += _ => moving = true;
        controls.Player.Move.canceled += _ => moveDirection = new Vector2(0, 0);

        controls.Player.FireWeapon.started += _ => shooting = true;
        controls.Enable();

        meleeWeapon.SetActive(false);
    }

    public void Quit(InputAction.CallbackContext context)
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;

#else
        Application.Quit();

#endif
    }


    //Receives Jump input action; if the player is on the ground set their jump direction to upwards
    public void Jump(InputAction.CallbackContext context)
    {
        if (grounded)  //Add an extra check here to allow for a buffer (so that players can jump 'prematurely')
        {
            jumpDirection.y = jumpHeight;
        }
    }

    public void GunDirection(InputAction.CallbackContext context)
    {
        gunDirection = controls.Player.GunDirection.ReadValue<Vector2>(); //Read joystick direction to a Vector3(direction)

        if (gunDirection.x != 0 && gunDirection.y != 0) //if not at start location, save direction to local variable
        {
            savedGunDirection = gunDirection;
        }
        //while hand is off joystick, GunDirection above will read 0,0,0 
        //saving the last direction before that lets the gun stay where you had it pointed
        float targetGunAngle = Mathf.Atan2(savedGunDirection.y, savedGunDirection.x) * Mathf.Rad2Deg;
        gun.rotation = Quaternion.Euler(0.0f, 0.0f, targetGunAngle);
    }

    public void DeployShield(InputAction.CallbackContext context)
    {
        GameObject newShield = (GameObject)Instantiate(shield, shieldInstantiatePoint.transform.position, new Quaternion(0f, 0f, 0f, 0f));
        newShield.GetComponent<DeployShield>().SetOwner(gameObject);
        newShield.GetComponent<DeployShield>().SetDirection(savedGunDirection.normalized);
    }

    //now unused
    public void OnFireWeapon(InputAction.CallbackContext context)
    {
        if(context.started)
        {
            shooting = true;
        }
        
        Debug.Log("Shot!");
        
    }

    public void ApplyGunForce()
    { 
        GameObject newBullet = (GameObject)Instantiate(bullet, bulletInstantiatePoint.transform.position, new Quaternion(0f, 0f, 0f, 0f)/*, (new Quaternion(savedGunDirection.x, savedGunDirection.y, 0f, 0f))*/);
        newBullet.GetComponent<MoveBullet>().SetDirection(savedGunDirection);
        newBullet.GetComponent<MoveBullet>().SetParameters(30.0f, 100.0f);  //later, range and maybe speed parameters will be set accoring to player variables
        newBullet.GetComponent<MoveBullet>().SetOwner(gameObject);          //sets the owner of the bullet for collision handling

        if (!grounded)
        {
            moveDirection += (-savedGunDirection * (gunRecoil * 0.25f));
        }
        else 
        {
            moveDirection += (-savedGunDirection * gunRecoil);
        }
        jumpDirection += (-savedGunDirection * airRecoilModifier);
        //shooting = false;
    }

    // public float Move()
    // {
    //     if (moving)
    //     {
    //         if (moveSpeed < maxMoveSpeed)
    //         {
    //             moveSpeed += acceleration;
    //         }
    //         else if (moveSpeed >= maxMoveSpeed)
    //         {
    //             if (controller.isGrounded)
    //             {
    //                 moveSpeed = maxMoveSpeed;
    //             }
    //         }
    //         Vector2 moveForce = controls.Player.Move.ReadValue<Vector2>();
    //         return moveForce.x;
    //     }

    //     return 0f;
    // }

    public void OnMove(InputAction.CallbackContext context)
    {
        moveDirection = context.ReadValue<Vector2>(); 
        moveDirection = moveDirection.normalized;

        if (moveDirection.x != 0f)
        {
            float targetBodyAngle = Mathf.Atan2(0.0f, moveDirection.x) * Mathf.Rad2Deg;
            float smoothedBodyAngle = Mathf.SmoothDampAngle(transform.localEulerAngles.y, targetBodyAngle, ref turnSmoothVelocity, turnSmoothing);
            transform.rotation = Quaternion.Euler(0.0f, smoothedBodyAngle, 0.0f);
        }
        
        //controller.Move(getMovement.x * Time.deltaTime * maxMoveSpeed);
    }

    // Update is called once per frame
    void Update()
    { 
        grounded = false;
        //colliders = Physics.OverlapSphere(new Vector2(transform.position.x, transform.position.y - 0.75f), 0.4f);
        colliders = Physics.OverlapBox(new Vector2(transform.position.x, transform.position.y - 0.6f), new Vector3(0.5f, 0.45f, 0.5f));
        
        foreach (var colliders in colliders)
        {
            if (colliders.tag == "Player")
            {
                continue;
            }
            else
            {
                Debug.Log("Touching Ground");
                grounded = true;
            }
        }

        if (!moving)
        {
            if (moveSpeed > 0)
            {
                moveSpeed = 0;
            }
            else if (moveSpeed < 0)
            {
                moveSpeed = 0f;
            }
        }
        
        #region mouse controls (not currently in use)
        //add landing particle effect
        //add landing audio effect
        /*
        //Cutrrently, this exhibits odd behaviour due to the wasy it's tracking mouse position within the Input System
        gunDirection = controls.Player.GunDirection.ReadValue<Vector2>();
        //Change screen coordinates to world coordinates (for correct muse behaviour)
        //returns a float range between 0 and 1 independent of screen size for x and y
        Vector3 mousePos = Camera.main.ScreenToViewportPoint(Mouse.current.position.ReadValue());
        //You'll need to introduce an in check in here based on whether or not players are using mouse or gamepad
        //Original line (presuable working for gamepads (TEST) below:
        //float targetGunAngle = Mathf.Atan2(gunDirection.y, gunDirection.x) * Mathf.Rad2Deg;

        
        float targetGunAngle = Mathf.Atan2(mousePos.y, mousePos.x) * Mathf.Rad2Deg;
        gun.rotation = Quaternion.Euler(0.0f, 0.0f, targetGunAngle);
        */
        #endregion


        //this should be moved out to a seperate function that decides force based on recoil and direction, then incrementaly moves based on force
        if (shooting && allowFire)
        {

            StartCoroutine(FireRate());

            IEnumerator FireRate()
            {
                allowFire = false;
                ApplyGunForce();
                yield return new WaitForSeconds(fireRate);
                allowFire = true;
            }
            //shooting = false;
        }


        if (moveDirection.y != 0)
        {
            moveDirection.y = 0f;
        }
        //Debug.Log("Move direction: " + moveDirection);
        controller.Move(moveDirection * Time.deltaTime * maxMoveSpeed);

        


        if (!grounded)
        {
            jumpDirection.y -= gravityMultiplier * Time.deltaTime;
        }

        if (jumpDirection.x != 0)
        {
            jumpDirection.x = 0f;
        }
        //Debug.Log("Jump Direction: " + jumpDirection);
        controller.Move(jumpDirection * Time.deltaTime);


        if (grounded)
        {
            jumpDirection.x = 0f;

            moveDirection.y = 0f;
            if (jumpDirection.y < 0f)
            {
                jumpDirection.y = 0f;
            }
        }

        //Debug.Log(transform.position);
        GetComponent<Transform>().SetPositionAndRotation(new Vector3(GetComponent<Transform>().position.x, GetComponent<Transform>().position.y, 0f), GetComponent<Transform>().rotation);


        //Debug.Log(moveDirection);

        //this should be moved out to a seperate function that decides force based on recoil and direction, then incrementaly moves based on force
        if (shooting && hasMeleeWeapon)
        {

            StartCoroutine(MeleeRate());

            IEnumerator MeleeRate()
            {
                allowMelee = false;
                Melee();
                yield return new WaitForSeconds(meleeRate);
                allowMelee = true;
            }
            shooting = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.tag == "Sword")
        {
            meleeWeapon.SetActive(true);
            //play pickup sound
            hasMeleeWeapon = true;
            allowFire = false;
            allowMelee = true;
            //Debug.Log("sword collected");
            //Need to destroy the sword as well
        }
    }

    void Melee()
    {
        Debug.Log("SWING!");
        moveDirection += (savedGunDirection * gunRecoil);
        jumpDirection += (savedGunDirection * airRecoilModifier);
        gun.rotation = Quaternion.Euler(0.0f, 0.0f, gun.rotation.z + 100.0f);
        /*        //This is object rotation, are we going to rotate the energy ball?
                GameObject newBullet = (GameObject)Instantiate(bullet, bulletInstantiatePoint.transform.position, new Quaternion(0f, 0f, 0f, 0f)*//*, (new Quaternion(savedGunDirection.x, savedGunDirection.y, 0f, 0f))*//*);
                newBullet.GetComponent<MoveBullet>().SetDirection(savedGunDirection);
                newBullet.GetComponent<MoveBullet>().SetParameters(30.0f, 100.0f);  //later, range and maybe speed parameters will be set accoring to player variables
                newBullet.GetComponent<MoveBullet>().SetOwner(gameObject);          //sets the owner of the bullet for collision handling
                //on shoot add recoil effect onto movement and jump

                moveDirection += (-savedGunDirection * gunRecoil);
                jumpDirection += (-savedGunDirection * airRecoilModifier);*/
    }
}
