﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class DualPlayerCamera : MonoBehaviour
{
    private Vector3 desiredPos;
    public List<Transform> Players;
    public float camSpeed;
    private new Transform transform;
    private Camera cam;


    void Start()
    {
        transform = GetComponent<Transform>();
        cam = GetComponent<Camera>();
    }

    private void Update()
    {
        //find all objects with player tag and store in "p"
        var newPlayers = GameObject.FindGameObjectsWithTag("Player");
        Players = new List<Transform>();
        //loop through all found players and add to player list
        for (int i = 0; i < newPlayers.Length; i++)
        {
            Players.Add(newPlayers[i].transform.GetComponent<Transform>());
        }
        
        desiredPos = Vector3.zero;
        float distance = 0f;

        //order by vertical height
        var heightSort = Players.OrderByDescending(p => p.position.y);
        //order by horizontal position
        var widthSort = Players.OrderByDescending(p => p.position.x);

        //find a pos between highest and lowest
        var midHeight = heightSort.First().position.y - heightSort.Last().position.y;

        //same as above, but on X (horizontal) axis
        var midWidth = widthSort.First().position.x - widthSort.Last().position.x;

        
        var distanceH = -(midHeight + 5f) * 0.5f / Mathf.Tan(cam.fieldOfView * 0.5f * Mathf.Deg2Rad);

        var distanceW = -(midWidth + 5f) * 0.5f / Mathf.Tan(cam.fieldOfView * 0.5f * Mathf.Deg2Rad);

        //if height distance is less than width distance, choose height as priority
        distance = distanceH < distanceW ? distanceH : distanceW;


        //add all player positions together
        for (int i = 0; i < Players.Count; i++)
        {
            desiredPos += Players[i].position;
        }
        //divide by number of players to average
        desiredPos /= Players.Count;

        //limit zoom (distance)
        if (distance > -10f)
        {
            distance = -10f;
        }

        //set "zoom" to calculated best distance
        desiredPos.z = distance;
    }

    void LateUpdate()
    {
        //move the camera
        transform.position = Vector3.MoveTowards(transform.position, desiredPos, camSpeed);
    }
}
