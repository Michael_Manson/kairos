﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeployShield : MonoBehaviour
{
    private AudioSource deploySound;
    private AudioSource undeploySound;
    private AudioSource breakSound;
    private AudioSource[] audioSources;

    private bool undeploySoundPlayed;
    private bool breakSoundPlayed;

    private GameObject barrier;
    private GameObject shieldBase;
    private BoxCollider collider;

    [SerializeField] private float barrierDelay = 1.0f;
    [SerializeField] private int shieldHealth = 100;
    [SerializeField] private float shieldDuration = 3.0f;
    private float timer = 0.0f;

    public GameObject owner;
    private Vector2 gunDirection;
    private float shieldRotation;


    // Start is called before the first frame update
    void Start()
    {
        shieldRotation = Mathf.Round(shieldRotation / 45.0f) * 45.0f;
        //You should have some way of clamping this rotation (to, say, incremenets of 45 degrees)
        transform.rotation = Quaternion.Euler(0.0f, 0.0f, shieldRotation);


        collider = GetComponent<BoxCollider>();
        collider.enabled = false;

        //Get the gameObjects of the two children
        barrier = gameObject.transform.GetChild(0).gameObject;
        shieldBase = gameObject.transform.GetChild(1).gameObject;

        //Get all of the audioSources attached to the shield
        audioSources = GetComponents<AudioSource>();
        deploySound = audioSources[0];
        undeploySound = audioSources[1];
        breakSound = audioSources[2];

        deploySound.Play();
        Invoke(nameof(RaiseBarrier), barrierDelay);

        undeploySoundPlayed = false;
        breakSoundPlayed = false;
    }

    // Update is called once per frame
    void Update()
    {   
        //Make the shield undeploy after a timer
        timer += Time.deltaTime;

        if (timer >= shieldDuration)
        {
            if (undeploySoundPlayed == false)
            {
                undeploySound.Play();
                undeploySoundPlayed = true;
            }

            PreparetoDestroy();
        }

        //Make the shield destroy when it has no health
        if (shieldHealth <= 0)
        {
            if (breakSoundPlayed == false)
            {
                breakSound.Play();
                breakSoundPlayed = true;
            }

            PreparetoDestroy();
        }
    }

    void RaiseBarrier()
    {
        barrier.SetActive(true);
        collider.enabled = true;
    }

    void PreparetoDestroy()
    {
        MeshRenderer barrierMesh = barrier.GetComponent<MeshRenderer>();
        MeshRenderer shieldBaseMesh = shieldBase.GetComponent<MeshRenderer>();
           
        barrierMesh.enabled = false;
        shieldBaseMesh.enabled = false;
        collider.enabled = false;
        //Destroy the shield after 2 seconds (time for audio to play)
        Invoke(nameof(DestroySelf), 1);
    }

    void DestroySelf()
    {
        Destroy(gameObject);
    }

    public void DeductHealth(int damage)
    {
        shieldHealth -= damage;
    }

    public void SetOwner(GameObject owner)
    {
        this.owner = owner;
    }

    public void SetDirection(Vector2 direction)
    {
        gunDirection = direction;
        shieldRotation = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
    }
}
