﻿using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    //currently, this will target the player
    //eventually, we'll want this to target a point between the two players
    //can take in a reference to each player transform, or create a special GameObject
    //that maintains a constant position between both players
    public Transform target;
    
    //the higher this value is, the faster the lock on
    [SerializeField] private float smoothing = 10.0f;
    [SerializeField] private Vector3 cameraOffset;

    //LateUpdate ensures the target has moved prior to the camera tracking them
    void LateUpdate()
    {
        //the actual position of the player transform
        Vector3 desiredPosition = target.position + cameraOffset;
        //the camera position, lagging behind the player
        Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothing * Time.deltaTime);
        transform.position = smoothedPosition; 
        transform.LookAt(target); //using this, we lose control over each axis of rotation; the effect is a bit odd right now; low smoothing values exacerbate this
    }

}
