**************************
PS4 Controller:
**************************

Walk - Left Joycon
Jump - L1

Aim - Right Joycon
Shoot - R2

Exit - Options

**************************
XBOX Controller:
**************************
Walk - Left Joycon
Jump - Left Bumper

Aim - Right Joycon
Shoot - Right Trigger

Exit - Start